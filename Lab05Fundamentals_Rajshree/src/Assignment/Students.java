package Assignment;

import java.util.Arrays;

public class Students {
	
	public static void main(String[] args) {
		String[] myClass = {"Bob", "Susan", "Alice"};
		
		//should be 3
		System.out.println(myClass.length);
		System.out.println(Arrays.toString(myClass));
		
		
		String[] myClass2 = new String[3];
		myClass2[0]= "Bob";
		myClass2[1]= "Susan";
		myClass2[2]= "Alice";
		
		System.out.println(myClass2.length);
		System.out.println(Arrays.toString(myClass2));
		
	
	}

}
